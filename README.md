# Sudoku #

A simple sudoku board game generator and solver. Deployed at http://pique.bitbucket.org/

## Code Organization ##

The project uses three libraries,

* jQuery for DOM manipulation and Utility methods,
* Handlebars for templating,
* Less for pre-processing CSS.

## Design Decisions ##
* Views And Models - Mostly following a backbone architecture where Views will depend on the models and reflect the changes.
* Pub/Sub for Models - This made the whole interaction between the views and the controllers very simple and dynamic.
* Reusable Views - Views are independent by itself and can be rendered multiple times and adding new views is trivial.
* Easily Extendable - Adding a feature for example is a standalone unit which will not affect any of the existing code.

## Problems Faced ##
* Scaling of the table.
* Interaction between the views - solved using pub.sub in the model.

## If I had more time ##
* Unit tests.
* keyboard Navigation.
* Better CSS scaling.
* Complete Hinter.
* Add a timer.
* Save the board.