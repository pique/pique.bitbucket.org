var SudokuBoardValidator = (function() {
    var _instance,
    
    validate = function(_board) {
        var allErrors = [],
            cellErrors;

        for (var row = 0; row < 9; row++) {
            for (var col = 0; col < 9; col++) {

                if (!_board.get(row, col) || !_board.get(row, col).value) {
                    continue;
                }

                cellErrors = validateCell(row, col, _board);
                if (!!cellErrors) {
                    allErrors = allErrors.concat(cellErrors);
                }
            }
        }

        _board.setErrors(allErrors);

        return allErrors;
    },

    validateCell = function(row, col, _board) {
        var rowErrors = _validateRow(row, col, _board),
            columnErrors = _validateCol(row, col, _board),
            sectionErrors = _validateSection(row, col, _board),
            allErrors = [];

        if (!!rowErrors) {
            allErrors = allErrors.concat(rowErrors.cells);
        }

        if (!!columnErrors) {
            allErrors = allErrors.concat(columnErrors.cells);
        }

        if (!!sectionErrors) {
            allErrors = allErrors.concat(sectionErrors.cells);
        }
        
        return allErrors;
    },

    _validateRow = function(row, col, _board) {
        var mesh = [],
            size = _board.getSize(),
            cellValue = _board.get(row, col).value,
            value;

        for (var i = 0; i < size; i++) {
            value = _board.get(row, i).value;

            if (!value || (i === col)) {
                continue;
            }

            mesh[value] = {row: row, col: i};
        }

        if (mesh[cellValue]) {
            /*clash*/
            return {
                error: 'ROW',
                index: row,
                cells: [{
                    row: row,
                    col: col
                }, {
                    row: mesh[cellValue].row,
                    col: mesh[cellValue].col
                }]
            }
        }

    },

    _validateCol = function(row, col, _board) {
        var mesh = [],
            size = _board.getSize(),
            cellValue = _board.get(row, col).value,
            value;

        for (var i = 0; i < size; i++) {
            value = _board.get(i, col).value;

            if (i === row) {
                continue;
            }

            mesh[value] = {row: i, col: col}
        }

        if (mesh[cellValue]) {
            /*clash*/
            return {
                error: 'COLUMN',
                index: col,
                cells: [{
                    row: row,
                    col: col
                }, {
                    row: mesh[cellValue].row,
                    col: mesh[cellValue].col
                }]
            }
        }

    },

    _validateSection = function(row, col, _board) {
        var mesh = [],
            size = _board.getSize(),
            sectionRow = Math.floor(row / 3),
            sectionCol = Math.floor(col / 3),
            cellSection = ((sectionRow * 3) + sectionCol),
            cellValue = _board.get(row, col).value,
            value;

        for (var i = 0; i < size; i++) {
            for (var j = 0; j < size; j++) {

                boxRow = Math.floor(row / 3);
                boxCol = Math.floor(col / 3);
                
                if (cellSection !== ((Math.floor(i / 3) * 3) + Math.floor(j / 3))) {
                    continue;
                }

                value = _board.get(i, j).value;

                if ((i === row) && (j === col)) {
                    continue;
                }

                mesh[value] = {
                    row: i,
                    col: j
                }
            }
        }

        if (mesh[cellValue]) {
            /*clash*/
            return {
                error: 'SECTION',
                index: cellSection,
                cells: [{
                    row: row,
                    col: col
                }, {
                    row: mesh[cellValue].row,
                    col: mesh[cellValue].col
                }]
            }
        }

    },

    _getInstance = function() {
        return {
            validate: validate
        }
    };

    return {
        getInstance: function() {
            if (!_instance) {
                _instance = _getInstance();
            }
            return _instance;
        }
    };
})();