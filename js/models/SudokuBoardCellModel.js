var BoardCell = function(row, col, value) {
    this.row = row;
    this.col = col;
    this.value = value;
    this.readOnly = false;
    this.solved = false;
    this.valid = true;
};