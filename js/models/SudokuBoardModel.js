var Board = function() {
    var _board = [],
        status = null,
        /* A simple pub/sub object */
        o = $({});

    _init = function () {
        for (var row = 0; row < 9; row++) {
            for (var col = 0; col < 9; col++) {
                set(row, col, null);
            }
        }

        setStatus('NEW');
    },

    getBoard = function() {
        /* just return a copy, not a reference */
        return $.extend(true, {}, _board);
    },

    get = function(row, col) {
        return _board[row][col];
    },

    set = function(row, col, value) {
        var cell = new BoardCell(row, col, value);
        _board[row] || (_board[row] = []);
        _board[row][col] = cell;
    },

    setCell = function(row, col, cell) {
        if (!cell) {
            return;
        }
        _board[row] || (_board[row] = []);
        _board[row][col] = cell;
    },

    setStatus = function(newStatus) {
        status = newStatus;
        publish('status', status);
    },

    getStatus = function() {
        return status;
    },

    getSize = function() {
        return 9;
    },

    setFlag = function(row, col, flag, value) {
        var cell = get(row, col);

        if (!cell) {
            return;
        }

        cell[flag] = value;
    },

    setErrors = function(errors) {
        var error = {
            errors: errors
        }

        errors.map(function(error) {
            var cell = setFlag(error.row, error.col, 'valid', false);
        });

        publish('errors', error);
    },

    setHint = function() {
        publish('status', 'HINTER');
    },

    listen = function() {
        o.on.apply(o, arguments);
    },

    stopListen = function() {
        o.off.apply(o, arguments);
    },

    publish = function() {
        o.trigger.apply(o, arguments);
    };

    _init();

    return {
        getBoard: getBoard,
        get: get,
        set: set,
        setCell: setCell,
        setHint: setHint,
        getStatus: getStatus,
        setStatus: setStatus,
        getSize: getSize,
        setFlag: setFlag,
        setErrors: setErrors,
        listen: listen,
        stopListen: stopListen
    }
};