var SudokuBoardSolver = (function() {
    var _instance,

    solve = function(_board) {
        for (var row = 0; row < 9; row++) {
            for (var col = 0; col < 9; col++) {
                _board.setFlag(row, col, 'valid', true);
            }
        }

        _solveBoard(_board);
    },


    _solveBoard = function(_board) {

        var emptyCell = _findEmptyCell(_board);

        if (!emptyCell) {
            return true;
        }
        var validKeys = _getValidKeys(emptyCell.row, emptyCell.col, _board);
        if (!!validKeys.length) {
            for (var i = 0; i < validKeys.length; i++) {
                _board.set(emptyCell.row, emptyCell.col, validKeys[i]);
                _board.setFlag(emptyCell.row, emptyCell.col, 'readOnly', true);
                _board.setFlag(emptyCell.row, emptyCell.col, 'solved', true);
                if (_solveBoard(_board)) {
                    return true;
                } else {
                    _board.set(emptyCell.row, emptyCell.col, null);
                    _board.setFlag(emptyCell.row, emptyCell.col, 'readOnly', false);
                    _board.setFlag(emptyCell.row, emptyCell.col, 'solved', false);
                }
            };
        } else {
            return false;
        }

    },

    _findEmptyCell = function(_board) {
        for (var row = 0; row < 9; row++) {
            for (var col = 0; col < 9; col++) {
                var cell = _board.get(row, col);
                if (cell && cell.readOnly) {
                    //_board.setFlag(row, col, 'valid', true);
                    continue;
                }
                return {
                    row: row,
                    col: col
                }
            }
        }
    },

    _getValidKeys = function(row, col, _board) {
        var validMesh = [1, 1, 1, 1, 1, 1, 1, 1, 1],
            validKeys = [],
            size = _board.getSize(),
            sectionRow = Math.floor(row / 3),
            sectionCol = Math.floor(col / 3),
            cellSection = ((sectionRow * 3) + sectionCol),
            value;

        for (var i = 0; i < size; i++) {
            value = _board.get(row, i).value;
            if (!value || (i === col)) {
                continue;
            }
            validMesh[value - 1] = 0;
        }

        for (var i = 0; i < size; i++) {
            value = _board.get(i, col).value;

            if (!value || (i === row)) {
                continue;
            }

            validMesh[value - 1] = 0;
        }

        for (var i = 0; i < size; i++) {
            for (var j = 0; j < size; j++) {

                var boxRow = Math.floor(row / 3);
                var boxCol = Math.floor(col / 3);

                if (cellSection !== ((Math.floor(i / 3) * 3) + Math.floor(j / 3))) {
                    continue;
                }

                value = _board.get(i, j).value;

                if ((i === row) && (j === col)) {
                    continue;
                }

                validMesh[value - 1] = 0;
            }
        }


        validMesh.map(function(key, index) {
            if (key === 1) {
                validKeys.push(index + 1);
            }

        });

        return validKeys;
    },

    _getInstance = function() {
        return {
            solve: solve
        }
    };

    return {
        getInstance: function() {
            if (!_instance) {
            _instance = _getInstance();
            }
            return _instance;
        }
    };
})();