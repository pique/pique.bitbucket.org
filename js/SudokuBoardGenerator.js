var SudokuBoardGenerator = (function() {
    var _instance,

    getNewBoard = function() {
        var board = new Board();

        board.set(0, 0, 5);
        board.set(0, 1, 3);
        board.set(0, 4, 7);
        board.set(1, 0, 6);
        board.set(1, 3, 1);
        board.set(1, 4, 9);
        board.set(1, 5, 5);
        board.set(2, 1, 9);
        board.set(2, 2, 8);
        board.set(2, 7, 6);
        board.set(3, 0, 8);
        board.set(3, 4, 6);
        board.set(3, 8, 3);
        board.set(4, 0, 4);
        board.set(4, 3, 8);
        board.set(4, 5, 3);
        board.set(4, 8, 1);
        board.set(5, 0, 7);
        board.set(5, 4, 2);
        board.set(5, 8, 6);
        board.set(6, 1, 6);
        board.set(6, 6, 2);
        board.set(6, 7, 8);
        board.set(7, 3, 4);
        board.set(7, 4, 1);
        board.set(7, 5, 9);
        board.set(7, 8, 5);
        board.set(8, 4, 8);
        board.set(8, 7, 7);
        board.set(8, 8, 9);


        board.setFlag(0, 0, 'readOnly', true);
        board.setFlag(0, 1, 'readOnly', true);
        board.setFlag(0, 4, 'readOnly', true);
        board.setFlag(1, 0, 'readOnly', true);
        board.setFlag(1, 3, 'readOnly', true);
        board.setFlag(1, 4, 'readOnly', true);
        board.setFlag(1, 5, 'readOnly', true);
        board.setFlag(2, 1, 'readOnly', true);
        board.setFlag(2, 2, 'readOnly', true);
        board.setFlag(2, 7, 'readOnly', true);
        board.setFlag(3, 0, 'readOnly', true);
        board.setFlag(3, 4, 'readOnly', true);
        board.setFlag(3, 8, 'readOnly', true);
        board.setFlag(4, 0, 'readOnly', true);
        board.setFlag(4, 3, 'readOnly', true);
        board.setFlag(4, 5, 'readOnly', true);
        board.setFlag(4, 8, 'readOnly', true);
        board.setFlag(5, 0, 'readOnly', true);
        board.setFlag(5, 4, 'readOnly', true);
        board.setFlag(5, 8, 'readOnly', true);
        board.setFlag(6, 1, 'readOnly', true);
        board.setFlag(6, 6, 'readOnly', true);
        board.setFlag(6, 7, 'readOnly', true);
        board.setFlag(7, 3, 'readOnly', true);
        board.setFlag(7, 4, 'readOnly', true);
        board.setFlag(7, 5, 'readOnly', true);
        board.setFlag(7, 8, 'readOnly', true);
        board.setFlag(8, 4, 'readOnly', true);
        board.setFlag(8, 7, 'readOnly', true);
        board.setFlag(8, 8, 'readOnly', true);

        board.setStatus('READY')

        return board;
    },

    generateBoard = function() {
        var genboard = new Board(),
            newboard,
            unique = [],
            rand,
            row,
            col;

        for (var i = 0; i < 9; i++ ) {

            rand = Math.floor((Math.random() * 9) + 1);

            if ($.inArray(rand, unique) >= 0) {
                continue;
            }

            unique.push(rand);

            genboard.set(i, i, rand);
        };

        SudokuBoardSolver.getInstance().solve(genboard);


        newboard = new Board();

        for (var i = 0; i < 35; i++ ) {

            row = Math.floor((Math.random() * 8));
            col = Math.floor((Math.random() * 8));

            
            newboard.set(row, col, genboard.get(row, col).value);
            newboard.setFlag(row, col, 'readOnly', true);
        };



        return newboard;
    },

    getEmptyBoard = function() {
        var board = new Board();
        return board;
    },

    _getInstance = function() {
        return {
            getNewBoard: getNewBoard,
            getEmptyBoard: getEmptyBoard,
            generateBoard: generateBoard
        }
    };

    return {
        getInstance: function() {
            if (!_instance) {
                _instance = _getInstance();
            }
            return _instance;
        }
    };
})();