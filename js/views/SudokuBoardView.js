var SudokuBoardView = function(options) {

    var _board,
        _element = options.element,
        _actionsView,
        _validator,
        _dialogElement = $('#dialogs'),
        _template = Handlebars.compile($("#sudoku-board-template").html()),
        _newTemplate = Handlebars.compile($("#sudoku-new-dialog-template").html()),

        _init = function() {
            _registerEvents();

            render();
        },

        _registerEvents = function() {
            var $sudoBoard = $('#board'),
                $sudokuCell = $('.sudoku-cell'),
                $newButton = $('#newgame');

            $sudoBoard.on('keypress', '.sudoku-cell', _clearCell);
            $sudoBoard.on('keyup', '.sudoku-cell', _updateCell);
            $sudoBoard.on('click', '.sudoku-cell', _clickCell);
            _dialogElement.on('click', '#newgame', _loadNewGame);
        },

        _loadNewGame = function() {
            _board = SudokuBoardGenerator.getInstance().generateBoard();
            _board.listen('status', _statusChange);
            _board.listen('errors', _handleErrors);

            _validator = new SudokuBoardValidator.getInstance();

            _actionsView = SudokuBoardActionsView({
                element: $('#actions'),
                board: _board
            });

            render();
        },

        _loadEndGameDialog = function() {
            _element.addClass('dim');
            new SudokuBoardEndActionView({
                element: _dialogElement,
                board: _board
            });
            _dialogElement.show();
        },

        _loadRulesDialog = function() {
            _element.addClass('dim');
            new SudokuBoardRulesActionView({
                element: _dialogElement,
                board: _board
            });
            _dialogElement.show();
        },

        _statusChange = function(event, newStatus) {
            switch (newStatus) {
                case 'END':
                    _loadEndGameDialog();
                    break;
                case 'SHOWRULES':
                    _loadRulesDialog();
                    break;
                case 'RESUME':
                    _element.removeClass('dim');
                    _clearDialog();
                    break;
                case 'END-CONFIRM':
                    _board = null;
                    _element.removeClass('dim');
                    _clearBoard();
                    render();
                    break;
                case 'SOLVED':
                    render();
                    break;
            }
        },

        _clearDialog = function() {
            _dialogElement.empty();
            _dialogElement.hide();
        },

        _clearBoard = function() {
            _element.empty();
            $('#actions').empty();
        },

        _findCell = function(row, col) {
            var $cell = $('#board .row' + row + ' .column' + col + ' .sudoku-cell');
            return $cell;
        },

        /* clear the cell so that the user doesnt have to clear the number manually */
        _clearCell = function(e) {
            var key = String.fromCharCode(e.which);

            if ((parseInt(key) > 0) && (parseInt(key) <= 9)) {
                $(this).val('');
            }

            
            e.preventDefault();
            e.stopPropagation();
        },

        _clickCell = function(e) {
            $('.active').removeClass('active');
            $(this).addClass('active');
            $(this).focus();
            e.preventDefault();
            e.stopPropagation()
        },

        _updateCell = function(e) {
            var key = String.fromCharCode(e.which),
                row,
                col;
            
            row = $(this).parent().parent().data('row');
            col = $(this).data('column');

            if (row === null || row === undefined) {
                return;
            }

            if (col === null || col === undefined) {
                return;
            }

            if (e.which === 8) {
                key = null;
                $(this).val(key);
                $(this).text(key);
                _board.set(row, col, key);
                _validator && _validator.validate(_board);
            } else if ((parseInt(key) > 0) && (parseInt(key) <= 9)) {
                $(this).val(key);
                $(this).text(key);
                _board.set(row, col, key);
                _validator && _validator.validate(_board);
            }
            e.preventDefault();
            e.stopPropagation()
        },

        _clearErrors = function() {
            $('.error').removeClass('error');
        },

        _handleErrors = function(event, errors) {
            var $cell,
                errorCells;

            if (!errors) {
                return;
            }

            errorCells = errors['cells'];
            _clearErrors();
            errors.errors.map(function(cell) {
                $cell = _findCell(cell.row, cell.col);

                $cell.addClass('error');
            });
        },

        render = function() {
            if (_board) {
                _clearDialog();
                _clearErrors();
                _element.html(_template({
                    board: _board.getBoard()
                }));
            } else {
                _dialogElement.html(_newTemplate());
            }
        };

    _init();

    return {

    }
};