var SudokuBoardActionsView = function(options) {

    var _element = options.element,
        _board = options.board,
        _solver  = SudokuBoardSolver.getInstance();
        _hinter = SudokuBoardHinter.getInstance();
        _template = Handlebars.compile($("#sudoku-actions-template").html()),

        _init = function() {
            render();
            _registerEvents();
        },

        _registerEvents = function() {
            _element.on('click', '#endgame', _endGame);
            _element.on('click', '#solve', _solveGame);
            _element.on('click', '#about', _aboutGame);
        },

        _endGame = function() {
            _board.setStatus('END');
        },

        _solveGame = function() {
            _solver.solve(_board);
            _board.setStatus('SOLVED');
        },

        _aboutGame = function() {
            _board.setStatus('SHOWRULES');
        },

        render = function() {
            _element.html(_template());
        };

    _init();

    return {
        render: render
    }
};