var SudokuBoardEndActionView = function(options) {

    var _element = options.element,
        _board = options.board,
        _template = Handlebars.compile($("#sudoku-end-dialog-template").html()),

        _init = function() {
            render();
            _registerEvents();
        },

        _registerEvents = function() {
            var $yesButton = $('#yes'),
                $noButton = $('#no');

            $yesButton.on('click', '', _endGame);
            $noButton.on('click', '', _resumeGame);
        },

        _endGame = function() {
            _board.setStatus('END-CONFIRM');
        },

        _resumeGame = function() {
            _board.setStatus('RESUME');
        },

        render = function() {
            _element.html(_template());
        };

    _init();

    return {
        render: render
    }
};