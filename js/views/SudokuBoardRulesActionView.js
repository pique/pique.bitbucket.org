var SudokuBoardRulesActionView = function(options) {

    var _element = options.element,
        _board = options.board,
        _template = Handlebars.compile($("#sudoku-rules-dialog-template").html()),

        _init = function() {
            render();
            _registerEvents();
        },

        _registerEvents = function() {
            var $resumeButton = $('#resume');

            $resumeButton.on('click', '', _resumeGame);
        },

        _resumeGame = function() {
            _board.setStatus('RESUME');
        },

        render = function() {
            _element.html(_template());
        };

    _init();

    return {
        render: render
    }
};