var SudokuBoardHinter = (function() {
    var _instance,

        hint = function(_board) {
            var _boardCopy = SudokuBoardGenerator.getInstance().getEmptyBoard(),
                _solver,
                _emptyCell;

            for (var row = 0; row < _board.getSize(); row++) {
                for (var col = 0; col < _board.getSize(); col++) {
                    _boardCopy.setCell(row, col, _board.get(row, col));
                }
            }

            _solver = new SudokuBoardSolver.getInstance();

            _solver.solve(_boardCopy);
            _emptyCell = _findHintCell(_board);
            
        },

        _findHintCell = function(_board) {
            for (var row = 0; row < 9; row++) {
                for (var col = 0; col < 9; col++) {
                    var cell = _board.get(row, col);
                    if (cell && cell.readOnly) {
                        continue;
                    }
                    return {
                        row: row,
                        col: col
                    }
                }
            }
        },

        _getInstance = function() {
            return {
                hint: hint
            }
        };

    return {
        getInstance: function() {
            if (!_instance) {
                _instance = _getInstance();
            }
            return _instance;
        }
    };
})();